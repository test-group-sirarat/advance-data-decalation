       IDENTIFICATION DIVISION. 
       PROGRAM-ID. USAGE-COMP2.
       AUTHOR. SIRARAT.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  A PIC S9(8) USAGE DISPLAY .
       01  AC  USAGE COMP-1  .
       01  AC1 PIC 9(4)V9(5) .
       01  COMPUTE-RESULT USAGE COMP-1  VALUE 06.234567891E-24.
       
       PROCEDURE DIVISION.
       BEGIN.
           INITIALIZE A 
           MOVE '1234567' TO A.
           MOVE '1999.99999' TO AC.
           MOVE  AC TO AC1.

           DISPLAY A.
           DISPLAY 'LENGTH ' LENGTH OF A.
           DISPLAY AC.
           DISPLAY AC1.
           DISPLAY 'LENGTH ' LENGTH OF AC.
           DISPLAY COMPUTE-RESULT .
           DISPLAY 'LENGTH ' LENGTH OF COMPUTE-RESULT 
           STOP RUN.
