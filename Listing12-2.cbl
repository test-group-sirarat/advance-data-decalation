       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Listing12-2.
       AUTHOR. SIRARAT YAIMADEUA.
      * RENAMES clasuse examples
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  STUDENT-REC.
           02 STUDENT-ID        PIC 9(8) VALUE 12345678.
           02 GPA               PIC 9V99 VALUE 3.25.
           02 FORENAME          PIC X(6) VALUE  "Matt".
           02 SURNAME           PIC X(8) VALUE "Cullen".
           02 Gender            PIC X VALUE "M".
           02 PHONE-NUMBER      PIC X(14) VALUE "353612022823".

       66  PERSONALINFO        RENAMES FORENAME THRU PHONE-NUMBER .
       66  COLLEGE-INFO        RENAMES STUDENT-ID THRU SURNAME .
       66  STUDENT-NAME        RENAMES FORENAME THRU SURNAME .

       01  CONTACT-INFO.
           02 STUD-NAME.
              03 STUD-FORENAME  PIC X(6).
              03 STUD-SURNAME   PIC X(8).
           02 STUD-GENDER       PIC X.
           02 STUD-PHONE        PIC X(14).

       66  MY-PHONE RENAMES STUD-PHONE .

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Example 1"
           DISPLAY  "All information = " STUDENT-REC 
           DISPLAY "College info  = " COLLEGE-INFO 
           DISPLAY "Personal Info  = " PERSONALINFO 

           DISPLAY "Example 2"
           DISPLAY "Combined names = " STUD-NAME 
           MOVE  PERSONALINFO TO CONTACT-INFO 

           DISPLAY "Example 3"
           DISPLAY "Name is  " STUD-NAME 
           DISPLAY "Gender is " STUD-GENDER 
           DISPLAY "Phone is " STUD-PHONE 

           DISPLAY "Example 4"
           DISPLAY "MyPhone is " MY-PHONE 
           STOP RUN 
           .  
          
       
          
